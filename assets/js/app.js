var _config = {
    activeClass: 'ui-state-active'
  };
function fullSize(){$(".fullheight").css({height:$(window).height()+"px"}),$(window).resize(function(){$(".fullheight").css({height:$(window).height()+"px"})}),$(".fullheightmore").css({height:$(window).height()+50+"px"}),$(window).resize(function(){$(".fullheightmore").css({height:$(window).height()+50+"px"})})}function playvid(){var t=$("#videodream").attr("src");$(".playvid").click(function(){$(window).scrollTop(0),$(".overflowvid").fadeIn("fast"),$("body").addClass("ovf"),$("#videodream").attr("src",t)})}function closevid(){$(".closevid").click(function(){$(".overflowvid").fadeOut("fast"),$("body").removeClass("ovf"),$("#videodream").attr("src","")})}function animbrain(){$(window).scroll(function(){$("#brainrep").each(function(t,i){var i=$(i);i.visible(!0)&&$(".animbrain").delay(1200).css({display:"block"},700)})})}function animstat(){$(".statanimknob").knob().delay(800),$(".statanimknob").val(0).trigger("change"),
$(".statanimknob").each(function(){return $(".boxstat.one .circlestat .statanimknob").delay(3500).animate({value:$(this).attr("data-val-after")},{duration:2e3,easing:"swing",progress:function(){$(this).val(Math.round(this.value)).trigger("change")},complete:function(){
var block = jQuery('.boxstat.one .loaderstat');
var disp = jQuery('span.display',block);
if(disp.length == 0)
{
  block.append('<span class="display">top 6</span>');
}
  }
}),$(".boxstat.two .circlestat .statanimknob").delay(5500).animate({value:$(this).attr("data-val-after")},{duration:2e3,easing:"swing",progress:function(){$(this).val(Math.round(this.value)).trigger("change")}}),
$(".boxstat.tree .circlestat .statanimknob").delay(7500).animate({value:$(this).attr("data-val-after")},{duration:2e3,easing:"swing",progress:function(){$(this).val(Math.round(this.value)).trigger("change")}}),!1})}function animnum(){return $("#numone").delay(3500).animateNumber({number:23000,numberStep: $.animateNumber.numberStepFactories.separator(' ')},2e3),
$("#numtwo").delay(5500).animateNumber({number:80,numberStep: $.animateNumber.numberStepFactories.separator(' ')},2e3),
$("#numtree").delay(7500).animateNumber({number:50,numberStep: $.animateNumber.numberStepFactories.separator(' ')},2e3),!1}function visiblestat(){$(window).scroll(function(){$("#statrep").each(function(t,i){var i=$(i);return i.visible(!0)&&firstTime?(firstTime=!1,animstat(),animnum(),!1):void 0})})}function animpin(){$(window).scroll(function(){$("#maprep").each(function(t,i){var i=$(i);i.visible(!0)&&(window.matchMedia("(min-height: 800px)").matches?$(".pin1").delay(200).fadeIn("fast").animate({bottom:"189px"},1100):$(".pin1").delay(200).fadeIn("fast").animate({bottom:"145px"},1100))}),$("#maprep").each(function(t,i){var i=$(i);i.visible(!0)&&(window.matchMedia("(min-height: 800px)").matches?$(".pin2").delay(400).fadeIn("fast").animate({bottom:"397px"},1100):$(".pin2").delay(400).fadeIn("fast").animate({bottom:"315px"},1100))})})}function loadinspin(){var t=document.getElementById("loader-app"),i=document.getElementById("border-app"),n=0,e=Math.PI,a=10;!function o(){n++,n%=360;var r=n*e/180,s=125*Math.sin(r),l=-125*Math.cos(r),d=n>180?1:0,c="M 0 0 v -125 A 125 125 1 "+d+" 1 "+s+" "+l+" z";t.setAttribute("d",c),i.setAttribute("d",c),setTimeout(o,a)}()}function removeitmobil(){$(window).width()<749&&$(".wrapper").detach()}!function(t){t.fn.visible=function(i){var n=t(this),e=t(window),a=e.scrollTop(),o=a+e.height(),r=n.offset().top,s=r+n.height(),l=i===!0?s:r,d=i===!0?r:s;return o>=d&&l>=a}}(jQuery);var firstTime=!0;$(window).load(function(){$(".sliderhome").flexslider({controlNav:!1,directionNav:!1})}),
$(document).ready(function(){

  jQuery('.about-us li').each(function()
  {
    var cItem = jQuery(this);

    var link = cItem.find('a').attr('href');

    cItem.append('<a href="'+link+'" class="block-link popin"></a>');
  });
  
  jQuery('header nav a').on('click',function()
  {
    var link = jQuery(this);
    
    jQuery('header nav a').removeClass(_config.activeClass);
    link.addClass(_config.activeClass);

    var id = link.attr("href");
    var scroll = link.data('deltaScroll');

    scrollToId(id,scroll);
    
    //return false;
  });

  function scrollToId(id)
  {
    console.log('id : '+id);
    var sT = parseInt(jQuery(id).offset().top);

    if(arguments.length >= 2)
    {
      var delta = parseInt(arguments[1]);
      
      if(!isNaN(delta))
      {
        sT += delta;
      }
    }
    //console.log('scrollTop : '+sT);
    jQuery('html, body').animate({
        scrollTop: sT
    }, 'slow');
  }

  jQuery(window).on('load',function()
  {
    var rawId = window.location.hash;

    if(rawId != '')
    {
      var scroll = jQuery('header nav a[href='+rawId+']').data('deltaScroll');
      window.setTimeout(scrollToId,700,rawId,scroll);
    }
  });

  function setTopBlockPosition()
  {
    //var topH = jQuery('.homescreen .inner').outerHeight();

    var headInfo = jQuery('.homescreen .headinfo');
    var mobileView = jQuery('.homescreen .mobilview');

    var topInfoH = headInfo.outerHeight();
    //console.log(topInfoH)
    var topMobileH = mobileView.outerHeight();

    var cssObj = {'top': '50%','bottom': 'auto'};

if(topInfoH != 0)
{
    cssObj['margin-top'] = -topInfoH/2;
    headInfo.css(cssObj);
  }

    cssObj['margin-top'] = -topMobileH/2;
    //mobileView.css(cssObj);
    
  }
  setTopBlockPosition();
  jQuery(window).on('resize',setTopBlockPosition);

  function activeVid()
  {
    jQuery('.playvid').addClass(_config.activeClass);
    window.setTimeout(resetVid,300);
  }
  function resetVid()
  {
    jQuery('.playvid').removeClass(_config.activeClass);
    window.setTimeout(activeVid,3000);
  }
  window.setTimeout(activeVid,6000);
  
  var winW = jQuery(window).width();
  var winH = jQuery(window).height();
  //jQuery("a.popin").prettyPopin({width: (winW/1.3),height: false,loader_path: 'assets/img/prettyPopin/loader.gif',followScroll: false/*,opacity: 1*/});

//jQuery("a.popin").colorbox({href: $(this).attr('href')});

  return fullSize(),playvid(),closevid(),animpin(),animbrain(),loadinspin(),visiblestat(),removeitmobil(),window.onload=function(){
    var sk = skrollr.init({forceHeight:!1});
    //skrollr.menu.init(sk);

  
    },!1
  });
